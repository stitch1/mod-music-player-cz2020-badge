# needs trailing slash :)
TRACK_SERVER = "http://5717.ch/public/czbadge/"

# key mapping:  (0 top left, 3 top right, 12 bottom left, etc.)
KEYS = {
    "previous_track": 12,
    "next_track": 15,

    "previous_animation": 13,
    "next_animation": 14,
}

"""
Track selection is done manually. There are SO MANY mods, of which there are many greats, and many terrible.

If you want to have your mods added, mail to elger.jonker@gmail.com and include the mods and genre.

All current mods are from modarchive.org, created by various artists.
"""

# get this with syncmods.py, see source over there
# sort them yourselves
PLAYLISTS = [
    {
        "name": ".: keygenmusic.net :.",
        "credits": "TOP 100 distribution by keygenmusic.net, music by respective/respected authors, see filenames",
        "directory": "keygenmusic_top100",
        "tracks": [
            "AT4RE - Multi Password Recovery 1.0.5crk.xm",
            "_) - WinRAR and RAR unblacklister.xm",
            "4DiAMONDS - Luxology Modo 1.03_15661crk.xm",
            "ACME - GetSmilekg.xm",
            "AGAiN - FairStars MP3 Recorderkg.XM",
            "AGAiN - TeleportProkg.xm",
            "AHTeam - Accent Office Password Recovery 2.12 (rus)crk.xm",
            "AiR - Acoustica Power Bundle 4.0kg.XM",
            "AiR - Celemony Melodyne Studio Edition 3.2.2.2kg.xm",
            "AiR - Propellerheads Reason 2002-2010 kg.xm",
            "AiR - Spectrasonics Omnispherekg.XM",
            "AvAtAr - RecoverMyFiles3.60crk.xm",
            "BetaMaster - Alcohol120_activator.xm",
            "BetaMaster - Alcohol120%v1.9.5.3105crk.xm",
            "BetaMaster - UltraISO 6.52crk.xm",
            "BLiZZARD - Sonic N intro.xm",
            "BRD - CHM2Wordkg.xm",
            "BRD - No.1 Video Converterkg.xm",
            "BRD - Teleport Pro kg.xm",
            "Cafe - 010 Editor 2.0kg.xm",
            "Canterwood - Hex Workshop v4.22kg.xm",
            "CORE - Adobe CS4 and CS5 kg v1.1.xm",
            "CORE - Adobe Dreamweaver CS4 10.0kg.xm",
            "Core - Pivot Pro v7.68kg.xm",
            "CORE - Power ISO 3.1kg.xm",
            "CORE - Steganos Internet Anonyme_1.XM",
            "CORE - Steganos Internet Anonyme_2.XM",
            "CORE - Winxpsilverkg.xm",
            "DEViANCE - Age Of Empires III v1.06 +16trn.xm",
            "DIGERATI - DVDIdlePro5.9.6.5crk.xm",
            "DYNAMITE - Winamp 5.0RC8crk.xm",
            "ECLiPSE - Ballleship Chess 2.0kg.XM",
            "Explosion - DVD to VCD SVCD MPEG AVI Converter 2.05kg.xm",
            "EXPLOSiON - Restorator2005v3.50.1442kg.xm",
            "FFF - EA Games Multikeygen 140.XM",
            "FFF - EA Games Multikg.xm",
            "FFF - Nero 7.xxkg.xm",
            "GradenT - Call of Duty Modern Warfare 3 1.9.433 +16 trn.xm",
            "H2O - Giga Studio 3.10 Orchestrakg.xm",
            "HERiTAGE - Teleport Pro 1.xkg.xm",
            "JUNLAJUBALAM - Adobe CS5activator.xm",
            "JUNLAJUBALAM - Dibac Plus Professional 2010 5.10.16 crk.xm",
            "Logic Impact - IntelliJ IDEA 2.5.2kg.xm",
            "Lz0 - Malwarebytes Anti-Malwarekg.xm",
            "MEMSMERiZE - ExifCleaner kg.XM",
            "MESMERiZE - GoodSync Prokg.XM",
            "ORiON - NeroBurningROM(2001).xm",
            "Orion-Nero6.3.0.2.xm",
            "Orion-Nero6.3.1.6.xm",
            "Orion-Nero6.6.xm",
            "PARADOX - 3dS MAX7.0kg.XM",
            "PARADOX - PhotoshopCS2kg.xm",
            "R2R - Ableton Live 1.2.2P kg.xm",
            "R2R - GuitarRig kg.xm",
            "R2R - IK Multimedia 1.0 kg.xm",
            "R2R - Image-Line kg v1.0.1.xm",
            "R2R - MakeMusic 1.0.7 kg.xm",
            "R2R - Native Instruments Kontakt 2.0.113 kg.xm",
            "R2R - Toontrack 3.0.1 kg.xm",
            "Razor1911 - Command and Conguer 3 Tiberium Warskg.xm",
            "Razor1911 - Crysiskg.xm",
            "Razor1911 - Soldier Of Fortune 2intro.XM",
            "RELOADED - Act Of War. High Treasonkg.XM",
            "Reloaded - Need For Speed Underground 2kg.XM",
            "RELOADED - Resident Evil 6 installer.xm",
            "RISE - Combustion4kg.xm",
            "SKiD ROW - Portal 2 launcher.xm",
            "SnD - IncrediMail for Office Generic Crack 1.5.xm",
            "SnD - Internet Download Manager 6.xx kg.xm",
            "TMG - CloneCD4.0.0.1kg.xm",
            "TMG - DocumentConverter4.682.xm",
            "tPORt - Cosmic Voyage Screensaver 3.2kg.xm",
            "TSRh - FlashGet1.65kg.xm",
            "Under SEH - ExifCleaner 1.2.2.29 kg.XM",
            "Vitality-BattleField2.xm",
            "XOR37H - BorlandC++Builder6kg.xm",
            # "AiR - eLicenser Emulatorinstaller.it",
            # "AiR - Truepianoskg.it",
            # "ArCADE - BluffTitler DX9 8.0.5kg.it",
            # "R2R - FabFilter products kg.it",
            # "R2R - Plugin Alliance 1.1 kg.it",
            # "R2R - Spectrasonics 2048 kg v1.1.2.it",
            "2000AD - Creatures To The Rescue +3 trn.mod",
            "Ackerlight - Academy-Tau Ceti II intro.mod",
            "ACME - Acronis TrueImage 10kg.mod",
            "ACME (Melboorn) - PC Flanc - Who Easykg.MOD",
            "AGRESSiON - Anti-Trojan Elite 3.4.3crk.mod",
            "CLASS - Hooligans.Storm Over Europe installer.mod",
            "Class - Seven Kingdoms 2intro.mod",
            "EPSiL0N - Ulead GIF Animator 5crk_3.mod",
            "EPSiL0N - Ulead GIF Animator 5crk_4.mod",
            "EPSiL0N - Ulead GIF Animator 5crk_5.mod",
            "EPSiL0N - Ulead GIF Animator 5crk_6.mod",
            "JUNLAJUBALAM - Vegas Movie Studio Platinum Edition 9.0b b92activator.MOD",
            "PiZZA - 7 Sins +5trn.MOD",
            "R2R - Xfer Records Products kg.mod",
            "Razor1911 - AD+D Ravensoftintro.MOD",
            "UPLiNK - SubiSoft Products kg.mod",
            "ORiGiN - Quake 3 Arenainstaller.s3m",
            "R2R - VirtualDJ 8 Pro Infinity kg.s3m",
        ]
    },
    # broken: 'an-path.xm', 'jt_strng.xm'
    {
        "name": "charts",
        "credits": "modarchive.org",
        "directory": "modarchive_top100",
        "tracks": [
            "artificial_sweetener.xm",
            "beyond_music.mod",
            "fall.xm",
            "elw-sick.xm",
            "A94FINAL.S3M",
            "AQUA.S3M",
            "playingw.mod",
            "aceman_-_my_first_console.xm",
            "tempest-acidjazz.mod",
            "CLICK.S3M",
            "knulla-kuk.mod",
            "loonie-biohazard.xm",
            "aws_aq16.xm",
            "2ND_SKAV.S3M",
            "corruption.mod",
            "kuk.xm",
            "alf2_zalza_edit.xm",
            "hours.xm",
            "xyce-dans_la_rue.xm",
            "external.xm",
            "CTGOBLIN.S3M",
            "BUTTERFL.XM",
            "purple_motion_-_sundance.mod",
            "FOUNTAIN.MOD",
            "enigma.mod",
            "scrambld.mod",
            "rainbowdash.xm",
            "radix-unreal_superhero.xm",
            "12TH.MOD",
            "isotoxin.s3m",
            "random_voice_-_monday.mod",
            "486.xm",
            "squired.xm",
            "space_debris.mod",
            "DOPE.MOD",
            "cream_of_the_earth.mod",
            "dragonatlas.xm",
            "class09.mod",
            "ambrozia.xm",
            "surfonasinewave.xm",
            "DEADLOCK.XM",
            "aftertouch.mod",
            "SHADOW.XM",
            "jt_911.xm",
            "world_of_plastic.s3m",
            "celestial_fantasia.s3m",
            "118in64.xm",
            "rollingdownthestreet.xm",
            "ninja.xm",
            "FEATSOFV.XM",
            "OCC-SAN.MOD",
            "the_snippet.xm",
            "KLISJE.MOD",
            "unk.xm",
            "2ND_PM.S3M",
            # "1_channel_moog.it",
            "asd.xm",
            "cloudcones.xm",
            "SHADOWRU.MOD",
            "SATELL.S3M",
            "2nd_reality.s3m",
            "stardstm.mod",
            "radix_-_the_mission.xm",
            "scotch.xm",
            # "chris31b.it",
            "aryx.s3m",
            "chcknbnk.mod",
            "mindrmr.xm",
            "radix-imaginary_friend.xm",
            "sweetdre.xm",
            "aurora-.mod",
            "legend.mod",
            # "verzion.it",
            "fap.xm",
            "aurora.mod",
            # "alone-mr.it",
            "knockerb.mod",
            "ELYSIUM.MOD",
            "toilet6.xm",
            "physical_presence.mod",
            "skogen11.mod",
            "street_j.mod",
            # "beyond_the_network.it",
            "pod.s3m",
            "echoing.mod",
            "i_realize_compo_edit.xm",
            "ICEFRONT.S3M",
            "boesendorfer_p_s_s.mod",
            "experience.xm",
            "galway.xm",
            "ascent.s3m",
            # "v17-revengeofcats.it",
            "titsfits.xm",
            "colours.xm",
            "1fineday.xm",
            "WHEN.S3M",
            "CHAOS.S3M",
            # "a-windf.it",
            "ICEFRONT(1).S3M",
            "4mat_-_eternity.xm",
            "dirt.mod",
            # "alex_brandon_-_deus_ex_title_music.it",
            "toilet4.xm",
            "projectx.mod",
            "jt_mind.xm",
            "unreeeal_superhero_3.xm",
            "strshine.s3m",
            "boo-come_along.xm",
            "superglam.xm",
            "jt_letgo.xm",
            "bananasplit.mod",
            "robocop3.xm",
            "_sunlight_.xm",
            "j-kgdsky.xm",
            "MECH8.S3M",
            "GSLINGER.MOD",
            "elw-hom2.xm",
        ],
    },
    {
        "name": "drum and bass",
        "credits": "modarchive.org",
        "directory": "drum_and_bass",
        "tracks": [
            # "a-polar.it", # does not support IT files
            "coretex_-_home.xm",
            "ctrix_-_fuxa7_stereo_mod_2007.mod",
            # "dcm-maxx.xm", not acepted
            "dream_shock.xm",
            # "floating_island_16_bit.xm", memory error.
            "logic.xm",
            "marble.xm",
            # "mechasha.it",
            # "neutral_capsule.xm", -> YMMV track, it will crash :)
            # "p703.it",
            # "tropical.it",
            # "85659-phainoma.it",
        ],
    },
    {
        # now: where are those HCM dj - brooklyn mob mods...
        "name": "hardcore",
        "credits": "modarchive.org, boozedrome",
        "directory": "hardcore",
        "tracks": [
            "thunderdome_8_m-mix.mod",
            "acn_rtth.mod",
            "veniek-indicator.xm",
            "buzzer-back_to_drome.mod",
            # "dest-h.it",
            "jzd_s666.mod",
            "kx_damnj.s3m",
        ],
    },
]
