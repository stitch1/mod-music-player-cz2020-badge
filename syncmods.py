# copy the mod directory to a server
# run with python3 syncmods.py
# python 3.6 compatible
import os
import subprocess

# syncmods upload options, this will upload your mods directory via SCP to some server of choice
MOD_DIRECTORY = "mods"
SERVER = "stitch@stitchbox:/var/www/5717.ch/public/czbadge"
VALID_EXTENSIONS = [".mod", ".xm", ".s3m"]


playlists = os.listdir(MOD_DIRECTORY)

for playlist in playlists:

    if os.path.isfile(playlist):
        continue

    filenames = os.listdir(f"{MOD_DIRECTORY}/{playlist}")

    def has_valid_extension(filename):
        filename, file_extension = os.path.splitext(filename)
        return True if file_extension.lower() in VALID_EXTENSIONS else False

    # only visible files:
    filenames = [filename for filename in filenames if not filename.startswith(".")]

    # only s3m, mod and xm are supported:
    filenames = [filename for filename in filenames if has_valid_extension(filename)]

    print(f"This is the code you need to add in __init__.py for your {playlist} mods:")
    print("----------------------------")
    print(filenames)
    print("----------------------------")

    command = ["rsync", "--progress", "--archive", "--verbose", "--delete", f"{MOD_DIRECTORY}/{playlist}", SERVER]

    process = subprocess.Popen(
        command,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        universal_newlines=True,
    )
    stdout, stderr = process.communicate()
    print(f"{stdout} {stderr}")
    print(f"Done uploading tracks. Might or might not have worked :)")
