# run pytest -vv in the project directory to run these tests.
import copy

from player import (
    next_track,
    set_playing_track,
    previous_track,
    vol,
    volume_lower,
    volume_raise,
    VOLUME_ADJUST_INTERVAL,
    get_current_volume,
    set_playlist,
    next_playlist,
    PLAYLISTS,
    get_random_track,
    current_playlist,
)


def test_volume():
    assert vol == 128
    initial_volume = copy.copy(vol)

    assert volume_raise() == initial_volume + VOLUME_ADJUST_INTERVAL

    assert volume_raise() == initial_volume + VOLUME_ADJUST_INTERVAL * 2

    assert volume_lower() == initial_volume + VOLUME_ADJUST_INTERVAL

    volume_lower()
    assert vol == initial_volume

    for i in range(0, 20):
        volume_lower()

    assert get_current_volume() == 0

    for i in range(0, 20):
        volume_raise()

    assert get_current_volume() == 255

    # one step lower should be dividable by 8 again
    assert volume_lower() == initial_volume + VOLUME_ADJUST_INTERVAL * 7


def test_playlist():
    assert PLAYLISTS[1] == next_playlist()

    set_playlist(PLAYLISTS[1])
    assert next_track() in PLAYLISTS[1]["tracks"]


def test_get_random_track():
    set_playlist(PLAYLISTS[0])

    assert PLAYLISTS.index(current_playlist) == 0

    assert get_random_track() in PLAYLISTS[0]["tracks"]


def test_next_track():
    assert len(PLAYLISTS[0]["tracks"]) > 2

    played = set()
    for i in range(0, len(PLAYLISTS[0]["tracks"]) + 10):
        thenext = previous_track()
        played.add(thenext)
        set_playing_track(thenext)
    assert sorted(PLAYLISTS[0]["tracks"]) == sorted(list(played))


def test_previous_track():
    played = set()
    for i in range(0, len(PLAYLISTS[0]["tracks"]) + 20):
        thenext = next_track()
        played.add(thenext)
        set_playing_track(thenext)
    assert sorted(PLAYLISTS[0]["tracks"]) == sorted(list(played))
