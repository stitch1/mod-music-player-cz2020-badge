"""
Display mock class, used for tests and development of visual effects.
"""

import tkinter
from time import sleep
from typing import List
from tkthread import tk, TkThread
import threading

labels: List[tkinter.Label] = []
width = height = [0, 1, 2, 3]
screen_buffer = [0xffffff, 0xffffff, 0xffffff, 0xffffff,
                 0xffffff, 0xffffff, 0xffffff, 0xffffff,
                 0xffffff, 0xffffff, 0xffffff, 0xffffff,
                 0xffffff, 0xffffff, 0xffffff, 0xffffff]


# https://github.com/serwy/tkthread
def run(func):
    threading.Thread(target=func).start()


root = None
tkt = None


def setup_window():
    global root, tkt
    root = tk.Tk()
    root.title("AERPANE CZ2020")
    root.geometry('600x600')
    root.grid()
    initial_colors = ['#00ff00', '#ff00ff', '#ff0000', '#ffff00', '#0000ff', '#00ff00', '#ff00ff', '#ff0000', '#ffff00',
                      '#0000ff', '#00ff00', '#ff00ff', '#ff0000', '#ffff00', '#0000ff', '#00ff00']
    for i in range(0, 16):
        label = tkinter.Label(root, anchor="nw", bg=initial_colors[i])
        label.grid(column=i % 4, row=i // 4, sticky='NSEW')
        labels.append(label)
    for i in range(0, 4):
        root.grid_columnconfigure(i, weight=1)
        root.grid_rowconfigure(i, weight=1)
    tkt = TkThread(root)
    tkt.nosync(root.wm_title, 'AERPANE 2020 - Simulating the pane...')
    root.update()
    # sleep(1)  # _tkinter.c:WaitForMainloop fails
    # root.mainloop()
    # exit("never reaching")


def swap_rb(hex_color):
    """
    Make the same mistake as in the firmware :)

    :param hex_color:
    :return:
    """
    r = hex_color >> 16 & 0b11111111
    g = hex_color >> 8 & 0b11111111
    b = hex_color >> 0 & 0b11111111

    return b << 16 | g << 8 | r


def kill_window():
    global root
    root.quit()


def drawPixel(x: int, y: int, color):
    global screen_buffer
    screen_buffer[y * len(height) + x] = color


def drawLine(x0, y0, x1, y1, color):
    # the stupidest line implementation ever :)
    drawPixel(x0, y0, color)
    drawPixel(x1, y1, color)


def orientation(angle):
    # and an even dumber orientation implementation :)
    pass


def getScreen():
    global screen_buffer
    return screen_buffer


def getPixel(x, y):
    global screen_buffer
    return swap_rb(screen_buffer[y * len(height) + x])


def drawFill(color):
    global screen_buffer
    screen_buffer = [color] * 16


def flush():
    global tkt, root
    for i in range(0, 16):
        # run(lambda: root.grid())
        # todo: simulator fills with purple, pink and green.
        labels[i]['bg'] = hex(getPixel(i % 4, i // 4)).replace("0x", "#").ljust(7, '0')

    root.update()
    sleep(0.1)
