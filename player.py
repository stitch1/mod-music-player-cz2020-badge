import random, sys

if not hasattr(sys, "_called_from_test"):
    from .rotated_list import forward, backward
    from .settings import PLAYLISTS
else:
    from rotated_list import forward, backward
    from settings import PLAYLISTS


vol = 128
VOLUME_ADJUST_INTERVAL = 16
MAXIMUM_VOLUME = 255

current_track = ""
current_playlist = PLAYLISTS[0]


def get_random_track():
    global current_playlist

    current_playlist_id = PLAYLISTS.index(current_playlist)

    return PLAYLISTS[current_playlist_id]["tracks"][
        random.randint(0, len(PLAYLISTS[current_playlist_id]["tracks"]) - 1)
    ]


def current_playlist_directory():
    return current_playlist['directory']


def next_playlist():
    global PLAYLISTS, current_playlist
    return forward(PLAYLISTS, current_playlist)


def previous_playlist():
    global PLAYLISTS, current_playlist
    return backward(PLAYLISTS, current_playlist)


def previous_track():
    global current_track, current_playlist
    return backward(current_playlist["tracks"], current_track)


def next_track():
    global current_track, current_playlist
    return forward(current_playlist["tracks"], current_track)


def volume_raise():
    global vol, VOLUME_ADJUST_INTERVAL
    vol += VOLUME_ADJUST_INTERVAL
    if vol > 255:
        vol = 255
    return vol


def volume_lower():
    global vol, VOLUME_ADJUST_INTERVAL

    # keep the complete range from 0 to 256, which is dividable by VOLUME_ADJUST_INTERVAL
    if vol == 255:
        vol = 256

    vol -= VOLUME_ADJUST_INTERVAL
    if vol < 0:
        vol = 0
    return vol


def set_current_volume(volume):
    global vol

    vol = volume


def get_current_volume():
    global vol
    return vol


def set_playing_track(track_name):
    global current_track
    current_track = track_name
    print("Now playing: " + current_track)


def set_playlist(playlist_data):
    global current_playlist, current_track
    current_playlist = playlist_data

    # set it to the last track of the list. So the "play next" will run the first track
    current_track = playlist_data["tracks"][len(playlist_data["tracks"]) - 1]
    print("Switching playlist: " + playlist_data["name"])
