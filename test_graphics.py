# run pytest -vv in the project directory to run these tests.
from copy import copy

import display
import graphics
from graphics import (
    state,
    sweep_frame, set_animation, get_next_animation,
)


def test_increase_sweep_state():
    assert state["sweep"]['position'] == 0

    sweep_frame()
    sweep_frame()

    assert state["sweep"]['position'] == 2

    sweep_frame()

    assert state["sweep"]['position'] == 3

    sweep_frame()
    assert state["sweep"]['position'] == 0

    initial_direction = copy(state["sweep"]["direction"])

    # test changing direction after a sweep
    for i in range(0, 1000):
        sweep_frame()
        if state["sweep"]["direction"] != initial_direction:
            continue

    assert state["sweep"]["direction"] != initial_direction

    # todo: add test startup in conftest, there can only be one root frame...
    # display.setup_window()
    # # 100 frames of sweep
    # for i in range(0, 16):
    #     sweep_frame()
    #     display.flush()


def test_matrix():

    display.setup_window()
    # try to show the graphic
    for i in range(0, 16):
        graphics.animate_matrix()
        display.flush()

    display.kill_window()


def test_set_animation():

    graphics.available_animations = ['a', 'b', 'c']
    graphics.state = {'current_animation': 'a'}
    set_animation(get_next_animation())

    assert graphics.state['current_animation'] == 'b'

    set_animation(get_next_animation())

    assert graphics.state['current_animation'] == 'c'

    set_animation(get_next_animation())

    assert graphics.state['current_animation'] == 'a'
