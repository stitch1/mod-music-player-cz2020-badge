# Mod Music Player

Plays some great tracks from modarchive,org.

Supports playlists. If you want your playlists with epic tracks to be added, send a mail to
elger.jonker@gmail.com with the MODS and the genre/playlist name. I'll create a new release.
Offer only valid during Campzone 2020.

Music from modarchive.org.


## Usage:

* Touchpad < and >: volume down and up
* Touchpad X and V: playlist back and forward
* Lower left and right buttons: previous and next track.
* Lower middle left and right buttons: previous and next animations.
* Any other bright button: switch colors, so be your own BPM visualization :)
* Home button: exit


## background
The challenge was to play some music, while keeping responsive buttons and changing colors. 
Mp3 at 24 or 48 kbps works fine and fits the fidelity of the speakers, but made the touchpad 
unresponsive unfortunately. The documentation stated that the badge also supported mod/xm/s3m, 
and that has of course very low cpu usage. You can even play a few songs at the same time.

See: https://docs.badge.team/esp32-app-development/api-reference/audio/


## changelog
Revision 4 adds a color fading touchpad

Revision 5 adds playlists, code restructuring, testcases, boot indicator, wifi needed indicator.

Revision 6 adds stability improvements for bad music files and bad wifi networks. Adds ± 100 tracks and a drum and bass plalyst.

Revision 7 uses multiple playlists, adds several new animations, the top 100 from keygenmusic.net. Cycle between animations using the bottom middle buttons.


## todolist:

- Support playing tracks directly from modarchive.com, just by referering their ID. So it's much easier to
create playlists (as no downloads/uploads are needed by the author). This does close the door to other mods, 
for example from competitions and such. It's probably easy to support both (int = modarchive, string = modserver).
- Support saving the current position of playlist and track.
- Is it possible to pre-cache a download (probably not due to size restrictions)
- Support various effects, instead of only fading colors
