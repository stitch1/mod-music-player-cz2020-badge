def forward(rotated_list, current_value):
    # not using valueerror exception handling here: the data in the app should be correct.
    current_index = rotated_list.index(current_value)

    if current_index == len(rotated_list) - 1:
        return rotated_list[0]
    else:
        return rotated_list[current_index + 1]


def backward(rotated_list, current_value):
    current_index = rotated_list.index(current_value)

    if current_index == 0:
        return rotated_list[len(rotated_list) - 1]
    else:
        return rotated_list[current_index - 1]
