from display import (
    getPixel,
    drawPixel,
    drawFill,
    getScreen
)


def test_mydisplay():

    drawFill(0xffffff)
    drawPixel(0, 0, 0x000000)

    assert getScreen() == [0x000000] + [0xffffff] * 15

    assert getPixel(0, 0) == 0x000000
